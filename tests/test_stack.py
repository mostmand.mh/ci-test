import unittest

from stack.stack import Stack


class TestStack(unittest.TestCase):
    def setUp(self) -> None:
        self.st = Stack()

    def make_stack_full(self):
        for i in range(0, 10):
            self.st.push(i)

    def test_is_empty(self):
        self.assertTrue(self.st.is_empty(), "The stack is not empty")
        self.assertRaises(IndexError, self.st.pop)

    def test_is_empty_when_stack_is_not_empty(self):
        self.st.push(1)
        self.assertFalse(self.st.is_empty(), "Stack is empty after pushing one element")

    def test_is_empty_when_stack_is_full(self):
        self.make_stack_full()
        self.assertFalse(self.st.is_empty(), "Stack is empty after pushing enough elements to make it full")

    def test_is_full_when_stack_is_empty(self):
        self.assertFalse(self.st.is_full(), "The stack is full when there is not any element in it")

    def test_is_full_when_stack_is_full(self):
        self.make_stack_full()
        self.assertTrue(self.st.is_full(), "The stack is not full")

    def test_is_full_when_stack_is_not_empty_and_not_full(self):
        self.st.push(1)
        self.assertFalse(self.st.is_full(), "Stack is full when there is not enough elements inside")

    def test_push_when_stack_is_full(self):
        self.make_stack_full()
        with self.assertRaises(OverflowError):
            self.st.push(10)

    def test_push_when_stack_has_one_element(self):
        self.st.push(1)
        self.st.push(2)
        self.assertEqual(2, self.st.top())

    def test_push_when_stack_is_empty(self):
        self.st.push(1)
        self.assertEqual(1, self.st.top())

    def test_push_element_other_than_int_raise_error(self):
        with self.assertRaises(TypeError):
            self.st.push(True)

    def test_push_none(self):
        with self.assertRaises(TypeError):
            self.st.push(None)

    def test_pop_when_stack_has_one_element(self):
        self.st.push(1)
        self.assertEqual(self.st.pop(), 1)
        self.assertTrue(self.st.is_empty())

    def test_pop_when_stack_is_full(self):
        self.make_stack_full()
        self.assertEqual(9, self.st.pop())
        self.assertFalse(self.st.is_full())

    def test_pop_when_stack_is_empty_raise_error(self):
        self.assertRaises(IndexError, self.st.pop)

    def test_top_when_stack_is_full(self):
        self.make_stack_full()
        self.assertEqual(9, self.st.top())
        self.assertTrue(self.st.is_full())

    def test_top_when_stack_is_empty_raise_error(self):
        self.assertRaises(IndexError, self.st.top)

    def test_top_when_stack_has_one_element(self):
        self.st.push(1)
        self.assertEqual(self.st.top(), 1)
        self.assertFalse(self.st.is_empty())

    def test_repr_when_empty(self):
        self.assertEqual(self.st.__repr__(), "Stack()")

    def test_repr_when_has_one_element(self):
        self.st.push(1)
        self.assertEqual(self.st.__repr__(), "Stack()")

    def test_repr_when_full(self):
        self.make_stack_full()
        self.assertEqual(self.st.__repr__(), "Stack()")

    def test_str_when_empty(self):
        self.assertEqual(self.st.__str__(), "Stack()")

    def test_str_when_has_one_element(self):
        self.st.push(1)
        self.assertEqual(self.st.__str__(), "Stack(1)")

    def test_str_when_full(self):
        self.make_stack_full()
        self.assertEqual(self.st.__str__(), "Stack(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)")


if __name__ == '__main__':
    unittest.main()
